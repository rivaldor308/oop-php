<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new animal("shaun");
echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "Cold Blooded : $sheep->cold_blooded <br><br>"; // "no"

$frog = new frog("buduk");
echo "Name : $frog->name <br>"; 
echo "Legs : $frog->legs <br>"; 
echo "Cold Blooded : $frog->cold_blooded <br>"; 
echo $frog->jump();

$sungokong = new ape("kera sakti");
echo "Name : $sungokong->name <br>"; 
echo "Legs : $sungokong->legs <br>"; 
echo "Cold Blooded : $sungokong->cold_blooded <br>"; 
echo $sungokong->yell();

?>